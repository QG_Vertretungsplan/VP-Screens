# VP Screens

This JavaScript project can be used to locally generate a website which displays the substitution plan. This project is aimed at digital signage - the substitution plans will be scrolled automatically.

The data which these scripts fetch have to be a JSON from the OpenSchulportfolio Time 2007 export plugin.

Copy the `settings.example.js` and name it `settings.js`. Then adjust the settings as needed.

By default, the `index.html` will show `day0` and `day1` (the first two days of the JSON). You can however also specify the days to be shown via a HTTP GET parameter: `index.html?daysToShow=0,1` would for example show the first two days, while `index.html?daysToShow=2` would only show the third day. This is useful if you dont want one big screen, but instead multiple smaller screens.