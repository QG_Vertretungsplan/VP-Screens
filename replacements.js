const replacements = [
    ["entfällt", "entfällt", "qgmvgreen"],
    ["Raumänderung", "Raumänderung", "qgmvorange"],
    ["Vertretung", "Vertretung", "qgmvred"],
    ["Zusatzstunde", "Zusatzstunde", "qgmvred"],
    ["Extrastunde", "Zusatzstunde", "qgmvred"],
    ["**", "Neu", "qgmvaktuell"]
];