/**
 * Address from which to fetch the plan.
 */
const planURL = "https://mein.portfolio.irgendwo/doku.php?timesub=getplan&timesubtoken=";
/**
 * Update interval (in seconds).
 */
const updateInterval = 300;
/**
 * If fetching the plan fails, the old plan will still be displayed.
 * This settings specifies the maximum age (in seconds) of the data before a warning will be displayed in addition to the plan itself.
 * Note that setting this to for example two times the value of updateInterval will probably only generate a warning message on the third update.
 */
const maxAge = 500;
/**
 * Duration in ms for which one view/page should be displayed before scrolling.
 * Note that this time frame already begins as soon as the animation for scrolling to the next page begins.
 */
const viewDuration = 15000;
/**
 * Duartion in ms the scroll animation should take. Set to 0 to disable the animation.
 */
const animationDuration = 3000;
/**
 * Whether to display the student ("aula") or teacher ("lehrer") version.
 */
const plantype = "aula";