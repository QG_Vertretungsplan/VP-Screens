/**
 * Scripts for resizing the table containers and for scrolling through the tables.
 */

/**
 * Interval for scrolling the plan tables.
 */
var scrollInterval;

/**
 * Resize the tables and start automatically scrolling them independently.
 * Will show each section for some amount of time (set in the settings).
 * When the bottom is reached it will jump back to the top.
 */
function resizeAndAddScrollers() {
  clearInterval(scrollInterval);
  const days = $("#days-container").children().children(".qgmv-plantable");
  let scrollFunctions = [];
  days.each((index, day) => {
    resizeTableContainer($(day));
    scrollFunctions.push(getScrollFunction($(day)));
  })

  scrollInterval = setInterval(function () {
    scrollFunctions.forEach(scrollFunction => scrollFunction());
  }, viewDuration);
}

/**
 * Calculates and updates the height of a qgmv-plantable container
 * so that it fits on the page without any overflow on the document body.
 * @param {jQuery} container container to resize
 * @returns {Number} the height that the container was set to.
 */
function resizeTableContainer(container) {
  const clientHeight = document.documentElement.clientHeight;
  const containerTop = container.position().top;
  const newHeight = `calc(${Math.floor(clientHeight - containerTop)}px - var(--page-margin))`; // consider the page margin
  container.height(newHeight);
  return newHeight;
}

/**
 * Get a function that will scroll the container to the next position. That function can be called repeatedly.
 * @param {jQuery} container container to scroll
 * @returns {function} A function that will scroll the container when called.
 */
function getScrollFunction(container) {
  const rowHeight = $("tr").outerHeight(); // FIXME: Just asking any tr is probably not the best idea.
  const containerHeight = container.height();
  // The amount of pixels to scroll each time
  const toScroll = rowHeight * Math.floor(containerHeight / rowHeight) - rowHeight;
  // The amount of pixels at which the container will be fully scrolled
  const scrollMaximum = container[0].scrollHeight - containerHeight;
  return () => {
    if (container.scrollTop() >= scrollMaximum) {
      // If the container was fully scrolled, go back to the top
      startScrollAnimation(container, 0);
    } else {
      // else, scroll down by "one page"
      const destination = Math.min(container.scrollTop() + toScroll, scrollMaximum)
      //container.scrollTop(destination);
      startScrollAnimation(container, destination);
    }
  }
}

/**
 * Scrolls the container to the given scrollTop destination.
 * The scrolling will be animated if specified in the settings.
 * @param {jQuery} container container to scroll.
 * @param {Number} destination scrollTop destination.
 */
function startScrollAnimation(container, destination) {
  if (animationDuration == 0) {
    container.scrollTop(destination);
    return;
  }

  let startTime = null;
  const startPos = container.scrollTop();
  const toTravel = destination - startPos;

  requestAnimationFrame(scrollAnimationStep);

  /**
   * Starts or continues the animation.
   * @param {Number} timestamp the timestamp for which to calculate the animation.
   */
  function scrollAnimationStep(timestamp) {
    if (!startTime) startTime = timestamp;
    const elapsed = timestamp - startTime;
    const progress = Math.min(elapsed / animationDuration, 1);
    const easedProgress = easeInOutCubic(progress);
    const newScrollTop = startPos + easedProgress * toTravel;
    container.scrollTop(newScrollTop);
    if (elapsed < animationDuration) {
      requestAnimationFrame(scrollAnimationStep);
    }
  }
}

/**
 * Cubic ease-in-out function.
 * @param {Number} t 0 <= t <= 1
 * @returns {Number} eased progress
 */
function easeInOutCubic(t) {
  return t < 0.5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1;
}