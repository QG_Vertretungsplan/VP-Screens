/**
 * Scripts for initializing the document and for regularly fetching and building the plan.
 */

/**
 * Unix timestamp since the plan was last updated.
 */
var lastPlanTime = 1;

/**
 * Array of the indices of the days to be displayed.
 */
var daysToShow = ["0", "1"];

/**
 * The string that was last fetched from the portfolio.
 */
var jsonString = null;

$(document).ready(function () {
  getDaysToShow();
  updateFontSize();
  fetchAndUpdate();
  // Regularly fetch plan data
  setInterval(function () {
    fetchAndUpdate();
  }, updateInterval * 1000);
});

window.addEventListener("resize", (event) => {
  updateFontSize();
  resizeAndAddScrollers();
});

/**
 * Updates the font size with respect to the client window size.
 */
function updateFontSize() {
  const displaywidth = document.documentElement.clientWidth;
  const fontsize = Math.floor(displaywidth / 109) + "px";
  document.body.style.fontSize = fontsize;
}

/**
 * Extracts the indices of the days to be displayed from the query parameter "daysToShow" if it exists.
 * This value will be saved in the global variable "daysToShow".
 * @returns {string[]} An array containing the indices of the days to be displayed.
 */
function getDaysToShow() {
  const urlParams = new URLSearchParams(window.location.search);
  const daysToShowParam = urlParams.get('daysToShow');
  if (daysToShowParam !== null) {
    daysToShow = daysToShowParam.split(",");
  }
  return daysToShow;
}

/**
 * Fetches the plan data from the web.
 * Builds a new substitution plan table and updates the page with it.
 */
function fetchAndUpdate() {
  $.getJSON(planURL, processFetchedData).fail(handleFetchError)
}

/**
 * Process the plan data that was fetched and parsed into an object.
 * Processing means building a plan out of it and displaying error messages if there are any errors.
 * @param {object} data The JSON plan data, parsed into an object.
 */
function processFetchedData(data) {
  const newDataString = JSON.stringify(data);
  if (newDataString !== jsonString) {
    try {
      const planHTML = buildVPTable(data, daysToShow);
      updateDaysContainer(planHTML);
    } catch {
      handleFetchError();
      return;
    }
    jsonString = newDataString;
  }
  removeWarnings();
  $("#days-container").children().width(`calc(${100 / daysToShow.length}% - 1em)`);
  resizeAndAddScrollers();
  lastPlanTime = Date.now();
}

/**
 * This function will add appropriate warnings for when fetching plan data or building a new plan has failed.
 */
function handleFetchError() {
  if (lastPlanTime != 1) {
    // There is still old plan data that can be shown
    if ((Date.now() - lastPlanTime) > (maxAge * 1000)) {
      // Display a warning if the data is too old.
      displayOldPlanWarning();
      resizeAndAddScrollers();
    }
  } else {
    // There is no old plan data -> display a warning.
    displayNoPlanWarning();
  }
}

/**
 * Displays a warning in the HTML, saying that there is no plan data.
 */
function displayNoPlanWarning() {
  if (!document.getElementById("qgmv-no-plan-warning")) {
    document.getElementById("warnings-container").innerHTML = '<div id="qgmv-no-plan-warning">Der Vertretungsplan ist gerade nicht abrufbar.</div>';
  }
}

/**
 * Display a warning in the HTML, saying that the current data is too old.
 */
function displayOldPlanWarning() {
  if (!document.getElementById("qgmv-old-plan-warning")) {
    const time = new Date(lastPlanTime);
    document.getElementById("warnings-container").innerHTML = `<div id="qgmv-old-plan-warning">Die Aktualisierung schlug fehl. Die letzte Aktualisierung war um ${time.toLocaleTimeString()} Uhr.</div>`;
  }
}

/**
 * Clears the warnings section.
 */
function removeWarnings() {
  document.getElementById("warnings-container").innerHTML = "";
}

/**
 * Sets the inner HTML of the days container to the given string.
 * @param {string} html HTML string to be set.
 */
function updateDaysContainer(html) {
  document.getElementById("days-container").innerHTML = html;
}
