/**
 * Builds the HTML for the plan from the JSON plan data.
 *
 * @param {object} data JSON object containing the plan data.
 * @param {string[]} dayIndices Array containing the indices of the days to show (e.g. ["0", "1"]).
 * @returns {string} HTML string for the selected days.
 */
function buildVPTable(data, dayIndices) {
  // Build the plan for each day.
  let daysHTML = "";
  for (dayIndex of dayIndices) {
    const day = "day" + dayIndex;
    // Do not show anything if the day doesn't exist.
    if (!Object.hasOwn(data, day)) {
      daysHTML += '<div> <div class="qgmv-no-data"> Für diesen Tag liegen keine Daten vor. </div> </div>';
      continue;
    }

    // build the div for that day.

    const tstattext = getEntry(data, day, "tstattext");
    const tdyntext = getEntry(data, day, "tdyntext");

    // div for the header with general information about that plan.
    const header = getHeaderHTML(tstattext);
    // Information about absent teachers, classes and courses and missing rooms. Only for the teacher version.
    const teacherInformation = (plantype == "lehrer") ? getTeacherInformation(tstattext) : [];
    const pleaseRegard = getPleaseRegard(tstattext);
    const table = getTable(tstattext, tdyntext);
    const dayContainer = createDayContainer(header, teacherInformation, pleaseRegard, table);
    daysHTML += dayContainer;
  }
  return daysHTML;
}

/**
 * Get the tdyntext or tstattext for the given day.
 * Also considers whether data for students (aula) or teachers (lehrer) should be used (from settings.js).
 * @param {object} data Object from the plan data JSON.
 * @param {string} day The selected day (e.g. "day0")
 * @param {string} type 'tdyntext' or 'tstattext'.
 * @returns {Array} The requested data from the plan data.
 */
function getEntry(data, day, type) {
  return data[day][type + plantype];
}

/**
 * Inserts the contents into a day container and returns the result.
 * @param {string} header Content of the header.
 * @param {string} absent Content of the teacher information section.
 * @param {string} pleaseRegard Content of the please regard.
 * @param {string} table Content of the table.
 * @returns {string} HTML for the day.
 */
function createDayContainer(header, absent, pleaseRegard, table) {
  let container = `
  <div class="qgmv-plan">
    <div class="qgmv-header">${header}</div>`;
  if (absent) {
    container += `<div class="qgmv-abw">${absent}</div>`;
  }
  if (pleaseRegard) {
    container += `<div class="qgmv-bibe">${pleaseRegard}</div>`;
  }
  container += `<div class="qgmv-plantable">${table}</div>      
  </div>
  `;
  return container;
}

/**
 * Builds the HTML for the header.
 * @param {Array} tstattext The tstattext array.
 * @returns {string} HTML string for the header.
 */
function getHeaderHTML(tstattext) {
  var html = `<div class="day-heading">${tstattext[0].Ueberschrift} ${tstattext[0].Datumlang}</div>`;
  html += `<div class="qgmv-ortinfo">${tstattext[0].Aushangort}</div>`;
  html += `<div class="qgmv-vinfo">${tstattext[0].Version} ${tstattext[0].Druckdatum}</div>`;
  return html;
}

/**
 * Builds an HTML containing absent teachers, classes and courses and missing rooms.
 * @param {Array} tstattext The tstattext array.
 * @returns {string} HTML string containing information for teachers.
 */
function getTeacherInformation(tstattext) {
  var html = "";
  if (tstattext[0].AbwKlassen != "") {
    html += "<span class = 'qgmv-abw-title'> Abwesende Klassen: </span>" + tstattext[0].AbwKlassen + "<br>";
  }
  if (tstattext[0].AbwKurse != "") {
    html += "<span class = 'qgmv-abw-title'> Abwesende Kurse: </span>" + tstattext[0].AbwKurse + "<br>";
  }
  if (tstattext[0].AbwLehrer != "") {
    html += "<span class = 'qgmv-abw-title'> Abwesende Lehrer: </span>" + tstattext[0].AbwLehrer + "<br>";
  }
  if (tstattext[0].FehlR\u00e4ume != "") {
    html += "<span class = 'qgmv-abw-title'> Fehlende Räume: </span>" + tstattext[0].FehlR\u00e4ume + "<br>";
  }
  return html;
}

/**
 * Get the please regard for the given day.
 * @param {Array} tstattext The tstattext array.
 * @returns {string} The HTML for the please regard or an empty string if there was no please regard.
 */
function getPleaseRegard(tstattext) {
  if (tstattext[0].BitteBeachten == "") {
    return "";
  }
  return `<strong>Bitte Beachten:</strong> ${tstattext[0].BitteBeachten}`;
}

/**
 * Get the table for the given day.
 * @param {Array} tstattext The tstattext array.
 * @param {Array} tdyntext The tdyntext array.
 * @returns {string} HTML for the table.
 */
function getTable(tstattext, tdyntext) {
  // Check if there are any substitutions and update the plan table accordingly.
  let html = "";
  if (typeof tstattext !== "undefined") {
    // Build the table.
    html = '<table class="qgmv-table" border=0>';

    html += getTableHeaderHTML(tstattext);

    // Build the table entries.
    var lastGrade = -1;
    for (var i = 0; i < tdyntext.length; i++) {
      //Finde heraus, wann sich die Stufe ändert
      const pattern = /\d+/;
      var curGrade = pattern.exec(tdyntext[i]["F1"]);
      if (curGrade !== null && plantype != "lehrer") {
        if (lastGrade[0] !== curGrade[0]) {
          lastGrade = curGrade;
          if (curGrade == 1) {
            html += `<tr class="qgmv-graderow"><td colspan=7>Jahrgangsstufe&nbsp;1</td></tr>`;
          } else if (curGrade == 2) {
            html += `<tr class="qgmv-graderow"><td colspan=7>Jahrgangsstufe&nbsp;2</td></tr>`;
          } else {
            html += `<tr class="qgmv-graderow"><td colspan=7>Klassenstufe&nbsp;${curGrade}</td></tr>`;
          }
        }
      }

      //Finde raus, ob die Zeile neu ist
      if (tdyntext[i].F7.includes("**")) {
        html += `<tr class="qgmv-newrow">`;
      } else {
        html += `<tr class="qgmv-substrow">`;
      }

      //Befülle die eigentlichen Spalten
      for (var j = 1; j <= 7; j++) {
        const field = "F" + j;
        html += `<td> ${makeReplacements(tdyntext[i][field])} </td>`;
      }
      html += `</tr>`;
    }

    html += `</table>`;
  }
  return html;
}

/**
 * Get the header for the table.
 * @param {Array} tstattext The tstattext array.
 * @returns {string} HTML string.
 */
function getTableHeaderHTML(tstattext) {
  var html = '<tr class="qgmv-headrow">';
  for (var i = 1; i <= 7; i++) {
    const field = "H" + i;
    const cellContent = tstattext[0][field];
    if (cellContent == "vertr. durch") {
      html += `<th class = "qgmv-vdurch"> ${cellContent} </th>`;
    } else {
      html += `<th> ${cellContent} </th>`;
    }
  }
  html += '</tr>';
  return html;
}

/**
 * Replaces parts of the string and gives it some HTML classes according to replacements.js
 * @param {string} text The string on which replacements should be done.
 * @returns {string} String with replacements.
 */
function makeReplacements(text) {
  replacements.forEach(replacement => {
    if (typeof replacement[2] !== "undefined") {
      text = text.replace(replacement[0], htmlTagText(replacement[1], "span", replacement[2]));
    } else {
      text = text.replace(replacement[0], replacement[1]);
    }
  });
  return text;
}

/**
 * Gives the string an HTML tag with given CSS classes.
 * @param {string} text String to be tagges.
 * @param {string} htmltag HTML tag to use.
 * @param {string} cssclass CSS class to use.
 * @returns
 */
function htmlTagText(text, htmltag, cssclass) {
  return `<${htmltag} class="${cssclass}">${text}</${htmltag}>`;
}